# disk-schedulers

Sets optimal disk schedulers per disk type:
* `cfq` for rotational hard disks (HDD)
* `noop` for solid state disks (SSD) connected via SATA

SSDs not connected via SATA are not subject to disk schedulers

The role auto-detects rotational and non-rotational disks.

## Example usage
No parameters, because of disk auto-detection.
```
- role: ansible-role-disk-schedulers
```

## Check current schedulers
```
grep . /sys/block/sd?/queue/scheduler
```
